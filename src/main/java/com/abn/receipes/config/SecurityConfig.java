package com.abn.receipes.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	 /**
	  * Used JDBC authentication for login
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
	        auth.jdbcAuthentication().passwordEncoder(passwordEncoder)
	            .dataSource(dataSource)
	            .usersByUsernameQuery("select username, password,'true' as enabled from user where username=?")
	            .authoritiesByUsernameQuery("select username, role from user where username=?");
	    }
	 
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	    	 http.authorizeHttpRequests()
	          .antMatchers("/h2-console/**","/user/**","/v3/api-docs/**","/swagger-ui.html/**","/swagger-ui/**")
	          .permitAll()
	          .anyRequest()
	          .authenticated()
	          .and()
	           .httpBasic()
	           .and()
	          .csrf().disable();
	        
	      
	        http.headers()
	          .frameOptions()
	          .sameOrigin();
	    }

}
