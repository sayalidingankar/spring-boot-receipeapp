package com.abn.receipes.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.abn.receipes.domain.Receipe;
import com.abn.receipes.response.ReceipeResponse;

public interface ReceipeService {
	
	public ResponseEntity<ReceipeResponse> createReceipe(Receipe receipe);

	public ResponseEntity<List<Receipe>> getReceipes();

	public ResponseEntity<ReceipeResponse> deleteReceipeById(Long id);

	public ResponseEntity<ReceipeResponse> deleteAllReceipes();

	public ResponseEntity<ReceipeResponse> updateReceipe(Long id, Receipe receipe);

}
