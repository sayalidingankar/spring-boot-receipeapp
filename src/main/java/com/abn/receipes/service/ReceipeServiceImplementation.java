package com.abn.receipes.service;



import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.abn.receipes.domain.Receipe;
import com.abn.receipes.exception.ResourceNotFoundException;
import com.abn.receipes.repository.ReceipeRepository;
import com.abn.receipes.response.ReceipeResponse;

@Service
public class ReceipeServiceImplementation implements ReceipeService{
	
	@Autowired
	ReceipeRepository receipeRepo;

	@Override
	public ResponseEntity<ReceipeResponse> createReceipe(Receipe receipe) {
		
		ReceipeResponse response= new ReceipeResponse();
		LocalDateTime dateTime=LocalDateTime.now();
		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd‐MM‐yyyy HH:mm");
		
		
		receipe.setCreatedDate(dateTime.format(formatter));
		receipeRepo.save(receipe);
		response.setMessage("Receipe added successfully");
		response.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity<>(response, HttpStatus.OK);
		
	}

	@Override
	public ResponseEntity<List<Receipe>> getReceipes() {
		
		List<Receipe> allreceipes=receipeRepo.findAll();
		
		return new ResponseEntity<>(allreceipes, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ReceipeResponse> deleteReceipeById(Long id) {
		Optional<Receipe> receipeOptional= receipeRepo.findById(id);
		ReceipeResponse response= new ReceipeResponse();
		if(receipeOptional.isPresent()) {
			Receipe receipe=receipeOptional.get();
			receipeRepo.delete(receipe);
		}else {
			throw new ResourceNotFoundException("Receipe not found");
		}
		response.setMessage("Receipe deleted successfully");
		response.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ReceipeResponse> deleteAllReceipes() {
		receipeRepo.deleteAll();
		ReceipeResponse response= new ReceipeResponse();
		response.setMessage("All receipes deleted successfully");
		response.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ReceipeResponse> updateReceipe(Long id, Receipe receipe) {
		Optional<Receipe> receipeOptional=receipeRepo.findById(id);
		Receipe receipeDb= null;
		if(receipeOptional.isPresent()) {
			receipeDb=receipeOptional.get();
			receipeDb.setCookingInstruction(receipe.getCookingInstruction());
			receipeDb.setIngredients(receipe.getIngredients());
			receipeDb.setVegIndicator(receipe.isVegIndicator());
			receipeDb.setRecipeName(receipe.getRecipeName());
			receipeDb.setServedForPeople(receipe.getServedForPeople());
			
			receipeDb=receipeRepo.save(receipeDb);
		}else {
			throw new ResourceNotFoundException("Receipe not found");
		}
		ReceipeResponse response= new ReceipeResponse();
		response.setMessage("Receipe updated successfully");
		response.setHttpStatus(HttpStatus.OK);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
