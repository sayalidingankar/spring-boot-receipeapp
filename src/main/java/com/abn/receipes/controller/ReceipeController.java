package com.abn.receipes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abn.receipes.domain.Receipe;
import com.abn.receipes.response.ReceipeResponse;
import com.abn.receipes.service.ReceipeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/receipes")
public class ReceipeController {
	
	
	@Autowired
	ReceipeService receipeService;
	
	/**
	 * Method to create recipe
	 * @param receipe
	 * @return ReceipeResponse
	 */
	@PostMapping("/createReceipe")
	public ResponseEntity<ReceipeResponse> createReceipe(@RequestBody Receipe receipe) {
		log.info("Creating receipe for "+receipe.toString());
		return receipeService.createReceipe(receipe);
		
	}
	
	/**
	 * Method to get all recipes
	 * @return list of recipes
	 */
	@GetMapping("/getReceipes")
	public ResponseEntity<List<Receipe>> getReceipes() {
		log.info("Get all recipes");
		return receipeService.getReceipes();
		
	}
	
	
	/**
	 * Method to delete recipe
	 * @param id
	 * @return ReceipeResponse
	 */
	@DeleteMapping("/deleteReceipe/{id}")
	public ResponseEntity<ReceipeResponse> deleteReceipe(@PathVariable Long id) {
		log.info("Delete recipe with id:"+ id);
		return receipeService.deleteReceipeById(id);
		
	}
	
	/**
	 * Method to delete all recipes
	 * @return ReceipeResponse
	 */
	@DeleteMapping("/deleteAllReceipes")
	public ResponseEntity<ReceipeResponse> deleteAllReceipes() {
		log.info("Delete all recipes");
		return receipeService.deleteAllReceipes();
		
	}
	
	/**
	 * Method to update recipe
	 * @param id
	 * @param receipe
	 * @return ReceipeResponse
	 */
	@PutMapping("/updateReceipe/{id}")
	public ResponseEntity<ReceipeResponse> updateReceipe(@PathVariable Long id, @RequestBody Receipe receipe) {
		log.info("update recipe with id :"+id);
		return receipeService.updateReceipe(id,receipe);
		
	}
	

}
