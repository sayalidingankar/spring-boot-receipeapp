package com.abn.receipes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abn.receipes.domain.User;
import com.abn.receipes.exception.UserAlreadyExistException;
import com.abn.receipes.repository.UserRepository;
import com.abn.receipes.response.ReceipeResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserRepository userRepo;
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@PostMapping("/register")
	public ResponseEntity<ReceipeResponse> createUser(@RequestBody User user) {
		log.info("Creating user with username :"+ user.getUsername());
		User userDB=userRepo.findByUsername(user.getUsername());
		if(userDB != null) {
			throw new UserAlreadyExistException("User already exist with given username");
		}
		String password=passwordEncoder.encode(user.getPassword());
		user.setPassword(password);
		userRepo.save(user);
		ReceipeResponse recipeRespone=new ReceipeResponse();
		recipeRespone.setHttpStatus(HttpStatus.OK);
		recipeRespone.setMessage("User registered successfully");
		return new ResponseEntity<ReceipeResponse>(recipeRespone, HttpStatus.OK);
		
		
	}
		
	

}
