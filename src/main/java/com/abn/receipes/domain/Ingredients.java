package com.abn.receipes.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="Ingredients")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ingredients {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String ingredientName;

	@Override
	public String toString() {
		return "Ingredients [id=" + id + ", ingredientName=" + ingredientName + "]";
	}
	
	

	
}
