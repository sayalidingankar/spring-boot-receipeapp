package com.abn.receipes.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="Receipe")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Receipe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String createdDate;
	
	@Column
	private String recipeName;
	
	@Column
	private boolean vegIndicator;
	
	@Column
	private String cookingInstruction;
	
	@Column
	private String servedForPeople;
	
	 @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	  @JoinColumn(name = "receipe_id", referencedColumnName = "id")
	private Set<Ingredients> ingredients;

	@Override
	public String toString() {
		return "Receipe [id=" + id + ", createdDate=" + createdDate + ", vegIndicator=" + vegIndicator
				+ ", cookingInstruction=" + cookingInstruction + ", ingredients=" + ingredients + "]";
	}

	

	

}
