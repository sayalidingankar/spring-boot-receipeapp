package com.abn.receipes.exception;

import org.springframework.http.HttpHeaders;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.abn.receipes.response.ExceptionResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.info("Exception occured :"+ ex.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        
        response.setErrorCode(HttpStatus.BAD_REQUEST);
        response.setErrorMessage(ex.getMessage());
        
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	
	
	@ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFoundException(ResourceNotFoundException ex) {
		log.info("Exception occured :"+ ex.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        
        response.setErrorCode(HttpStatus.NOT_FOUND);
        response.setErrorMessage(ex.getMessage());
        
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(UserAlreadyExistException.class)
    public ResponseEntity<ExceptionResponse> userAlreadyExistException(UserAlreadyExistException ex) {
		log.info("Exception occured :"+ ex.getMessage());
		ExceptionResponse response=new ExceptionResponse();
        
        response.setErrorCode(HttpStatus.FOUND);
        response.setErrorMessage(ex.getMessage());
        
        return new ResponseEntity<>(response, HttpStatus.FOUND);
    }
	
	
}
