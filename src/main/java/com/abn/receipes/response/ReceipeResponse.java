package com.abn.receipes.response;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReceipeResponse {
	
	private HttpStatus httpStatus;
	
	private String message;

	

}
