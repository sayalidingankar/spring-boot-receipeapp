package com.abn.receipes.response;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {

	
    private HttpStatus errorCode;
	
	private String errorMessage;
	
	

	
}