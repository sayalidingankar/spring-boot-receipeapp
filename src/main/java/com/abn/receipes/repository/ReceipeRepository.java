package com.abn.receipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.abn.receipes.domain.Receipe;

@Repository
public interface ReceipeRepository extends JpaRepository<Receipe, Long>{

}
