package com.abn.receipes.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {
	
	@Test
	public void userPojotest() {
		User user=new User();
		user.setId(1);
		user.setUsername("Sayali");
		user.setPassword("password");
		user.setRole("Admin");
		assertEquals(user.getId(), 1);
		assertEquals(user.getUsername(), "Sayali");
		assertEquals(user.getPassword(), "password");
		assertEquals(user.getRole(), "Admin");
	}
	
	@Test
	public void userEqualHashcodeTest() {
		User user1=new User(2,"Sayali","password","Admin");
		User user2=new User(2,"Sayali","password","Admin");
		assertEquals(user1, user2);
		assertThat(user1.hashCode() == user2.hashCode());
	}

}
