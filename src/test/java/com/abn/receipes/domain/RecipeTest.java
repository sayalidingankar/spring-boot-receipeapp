package com.abn.receipes.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RecipeTest {
	
	@Test
	public void recipePojoTest() {
		HashSet<Ingredients> ingredientSet = new HashSet<>();
		ingredientSet.add(new Ingredients(15, "masala"));
		Receipe recipe=new Receipe();
		recipe.setCookingInstruction("ABCDD");
		recipe.setCreatedDate("17/03/2022");
		recipe.setId(1);
		recipe.setVegIndicator(true);
		recipe.setIngredients(ingredientSet);
		assertEquals(recipe.getCookingInstruction(), "ABCDD");
		assertEquals(recipe.getCreatedDate(), "17/03/2022");
		assertEquals(recipe.getId(), 1);
		assertEquals(recipe.isVegIndicator(), true);
		assertThat(recipe.getIngredients().contains(new Ingredients(15, "masala")));
		
		
	}
	@Test
	public void recipeEqualsHashcodeTest() {
		HashSet<Ingredients> ingredientSet = new HashSet<>();
		ingredientSet.add(new Ingredients(15, "masala"));
		Receipe recipe1=new Receipe(1, "17/03/2022","pasta", false, "ABCDD","6", ingredientSet);
		Receipe recipe2=new Receipe(1, "17/03/2022","pasta", false, "ABCDD","6", ingredientSet);
		assertEquals(recipe1,recipe2);
		assertTrue(recipe1.hashCode()==recipe2.hashCode());
	}
	
	

}
