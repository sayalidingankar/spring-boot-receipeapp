package com.abn.receipes;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.abn.receipes.controller.ReceipeController;

@SpringBootTest
class RecipeApplicationTests {
	
	@Autowired
	ReceipeController receipeController;

	@Test
	void contextLoads() {
		assertThat(receipeController).isNotNull();
	}

}
