package com.abn.receipes.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.abn.receipes.domain.Ingredients;
import com.abn.receipes.domain.Receipe;
import com.abn.receipes.repository.ReceipeRepository;

import com.google.gson.Gson;

@SpringBootTest
@AutoConfigureMockMvc

public class ReceipeControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	ReceipeRepository recipeRepo;
	
	@Test
	public void forbiddenTest() throws Exception {
		this.mockMvc.perform(get("/receipes/getReceipes")).andDo(print()).andExpect(status().isUnauthorized());
	}

	@Test
	@WithMockUser
	public void getRecipesTest() throws Exception {
		this.mockMvc.perform(get("/receipes/getReceipes")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	@WithMockUser
	public void createRecipeTest() throws Exception {
		HashSet<Ingredients> ingredientSet = new HashSet<>();
		ingredientSet.add(new Ingredients(10, "masala"));

		mockMvc.perform(MockMvcRequestBuilders.post("/receipes/createReceipe")
				
				.content(new Gson().toJson(new Receipe(1, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet)))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(containsString("Receipe added successfully")));
	}
	
	
	  @Test
	  @WithMockUser 
	  public void updateReceipeTest() throws Exception {
		  HashSet<Ingredients> ingredientSet = new HashSet<>();
		  ingredientSet.add(new Ingredients(15, "masala"));
	      Optional<Receipe> receipe=Optional.of(new Receipe(10, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet));
		  when(recipeRepo.findById(10L)).thenReturn(receipe);
		
		
	    mockMvc.perform( MockMvcRequestBuilders
			  		.put("/receipes/updateReceipe/10") 
			  		.content(new Gson().toJson(new Receipe(11, LocalDateTime.now().toString(),"pasta", true, "rythggjh6677","6", ingredientSet)))
			  		.contentType(MediaType.APPLICATION_JSON)
			  		.accept(MediaType.APPLICATION_JSON))
	  				.andExpect(status().isOk())
	  				.andExpect(content().string(containsString("Receipe updated successfully")));
	  }
	  
	  @Test
	  @WithMockUser 
	  public void updateReceipeBadRequestTest() throws Exception {
		  HashSet<Ingredients> ingredientSet = new HashSet<>();
		  ingredientSet.add(new Ingredients(15, "masala"));
	      Optional<Receipe> receipe=Optional.of(new Receipe(10, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet));
		  when(recipeRepo.findById(10L)).thenReturn(receipe);
		
		
	    mockMvc.perform( MockMvcRequestBuilders
			  		.put("/receipes/updateReceipe/10") 
			  		.contentType(MediaType.APPLICATION_JSON)
			  		.accept(MediaType.APPLICATION_JSON))
	  				.andExpect(status().isBadRequest());
	  				
	  }
	  
	  @Test
	  @WithMockUser 
	  public void updateReceipeNotFoundTest() throws Exception {
	    mockMvc.perform( MockMvcRequestBuilders
			  		.put("/receipes/updateReceipe/19") 
			  		.content(new Gson().toJson(new Receipe(19, LocalDateTime.now().toString(),"pasta", true, "rythggjh6677","6", null)))
			  		.contentType(MediaType.APPLICATION_JSON)
			  		.accept(MediaType.APPLICATION_JSON))
	  				.andExpect(status().isNotFound())
	  				.andExpect(content().string(containsString("Receipe not found")));
	  }
	 

	
	  @Test
	  @WithMockUser 
	  public void deleteReceipeTest() throws Exception {
		  HashSet<Ingredients> ingredientSet = new HashSet<>();
		  ingredientSet.add(new Ingredients(15, "masala"));
	      Optional<Receipe> receipe=Optional.of(new Receipe(11, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet));
		  when(recipeRepo.findById(11L)).thenReturn(receipe);
	   
		  mockMvc.perform( MockMvcRequestBuilders
			  		.delete("/receipes/deleteReceipe/11") 
			  		.contentType(MediaType.APPLICATION_JSON)
			  		.accept(MediaType.APPLICATION_JSON))
	  				.andExpect(status().isOk())
	  				.andExpect(content().string(containsString("Receipe deleted successfully")));
	  }
	 
	  
	  @Test
	  @WithMockUser 
	  public void deleteReceipeNotFoundTest() throws Exception {
		  mockMvc.perform( MockMvcRequestBuilders
			  		.delete("/receipes/deleteReceipe/5") 
			  		.contentType(MediaType.APPLICATION_JSON)
			  		.accept(MediaType.APPLICATION_JSON))
	  				.andExpect(status().isNotFound())
	  				.andExpect(content().string(containsString("Receipe not found")));
	  }

	
	  @Test
	  @WithMockUser 
	  public void deleteAllReceipeTest() throws Exception {
		  HashSet<Ingredients> ingredientSet = new HashSet<>();
		  ingredientSet.add(new Ingredients(15, "masala"));
	      Optional<Receipe> receipe=Optional.of(new Receipe(12, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet));
		  when(recipeRepo.findById(12L)).thenReturn(receipe);
		  Optional<Receipe> receipe2=Optional.of(new Receipe(13, LocalDateTime.now().toString(),"pasta", true, "rythggjh","6", ingredientSet));
		  when(recipeRepo.findById(13L)).thenReturn(receipe2);
	   mockMvc.perform( MockMvcRequestBuilders
			  .delete("/receipes/deleteAllReceipes")
			  .accept(MediaType.APPLICATION_JSON)) .andExpect(status().isOk());
	   
	   assertTrue(recipeRepo.findAll().size() == 0);
	  		  
	  }
	 

}
