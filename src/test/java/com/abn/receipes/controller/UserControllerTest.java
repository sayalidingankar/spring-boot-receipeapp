package com.abn.receipes.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.Optional;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.abn.receipes.domain.User;
import com.abn.receipes.repository.UserRepository;
import com.google.gson.Gson;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	UserRepository userRepo;
	
	
	@Test
	public void registerUserTest() throws Exception {
	
		  
		  mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
					.content(new Gson().toJson(new User(4, "Sayali", "password", "ADMIN")))
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
	}
	
	@Test
	public void alreadyRegisteredUserTest() throws Exception {
		 User user=new User(10,"Sayali","password","admin");
		 when(userRepo.findByUsername("Sayali")).thenReturn(user);
		  
		  mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
					.content(new Gson().toJson(new User(10,"Sayali","password","admin")))
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isFound());
	}
}
