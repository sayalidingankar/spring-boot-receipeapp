# spring-boot-receipeapp



## OverView

spring-boot-receipeapp is developed as part of ABN Amro technical assessment. The web application contains CRUD operations to get,create,update and delete recipe from database

## System Design

1) Web application is created using Spring boot and JPA repositories are used for interation with database.
2) Application is using in memory H2 database
3) Basic Authentication is implemented using Spring security JDBC authentication provider.

## Technologies used
  Rest API-Java 8,Spring boot
  ORM- Spring boot JPA
  Junit- Spring boot with 
  Logging- Lombok
  Deployment-Docker
  
  




